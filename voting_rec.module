<?php

/**
 * @see http://drupal.org/project/voting_rec
 */

/**
 * Implementation of hook_help().
 */
function voting_rec_help($path, $args) {
  $output = '';
  switch ($path) {
    case "admin/help#voting_rec":
      $output = '<p>'.  t("This module generates node recommendations based on votes from Vote Up/Down and Plus 1") .'</p>';
      break;
  }
  return $output;
}

/**
 * Implementation of hook_perm().
 */
function voting_rec_perm() {
  $perm = array(
    "administer recommender",
  );
  return $perm;
}


/**
 * Implementation of hook_menu().
 */
function voting_rec_menu() {
  $items = array();
  $items['admin/settings/voting_rec'] = array(
    'title' => t('Points voting recommender'),
    'description' => 'Configuration and adjusts of recommender based on votings from Vote Up/Down or Plus 1',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('voting_rec_settings_form'),
    'access arguments' => array('administer recommender'),
    'file' => 'voting_rec.admin.inc',
   );
  return $items;
}


/**
 * Implementation of hook_block()
 */
function voting_rec_block($op='list', $delta=0) {
  if ($op == 'list') {
    if (variable_get('voting_rec_enable_user2user', 1)) {
      $block[0]['info'] = t('Points voting recommender: similar users');
      $block[1]['info'] = t('Points voting recommender: suggestions from similar users');
    }
    if (variable_get('voting_rec_enable_item2item', 1)) {
      $block[2]['info'] = t('Points voting recommender: similar items by votes');
      $block[3]['info'] = t('Points voting recommender: suggestions from voting history');
    }
    return $block;
  }
  else if ($op == 'view') {
    $block = array();
    $display_num = variable_get('voting_rec_display_num', 5);
    $same_type = variable_get('voting_rec_same_type', 1);

    // similar users block
    if ($delta == 0) {
      global $user;
      if ($user->uid == 0) return;  // we don't display stuff for anonymous users
      $user_list = recommender_top_similarity('voting_rec_user2user', $user->uid, $display_num);
      if (!empty($user_list)) {
        foreach ($user_list as $a_user) {
          $display[] = theme('username', user_load($a_user['id']));
        }
        $block['subject'] = t('Users who voted similarly as you');
        $block['content'] = theme('item_list', $display);
      }
    }
    // suggestions from similar users
    elseif ($delta == 1) {
      global $user;
      if ($user->uid == 0) return;  // we don't display stuff for anonymous users
      $item_list = recommender_top_prediction('voting_rec_user2user', $user->uid, $display_num, '_voting_rec_enforce_same_type');
      if (!empty($item_list)) {
        foreach ($item_list as $item) {
          $node = node_load($item['id']);
          $display[] = l($node->title, "/node/{$node->nid}");
        }
        $block['subject'] = t('Recommended nodes from users who voted similarly as you');
        $block['content'] = theme('item_list', $display);
      }
    }
    // similar items by votes
    elseif ($delta == 2) {
      if ($current_node = menu_get_object() && _voting_rec_check_content_type($current_node->type)) {
        $item_list = recommender_top_prediction('voting_rec_item2item', $current_node->nid, $display_num, '_voting_rec_enforce_same_type');
        if (!empty($item_list)) {
          foreach ($item_list as $item) {
            $node = node_load($item['id']);
            $display[] = l($node->title, "/node/{$node->nid}");
          }
          $block['subject'] = t('Other nodes voted similar to this one');
          $block['content'] = theme('item_list', $display);
        }
      }
    }
    // suggestions from voting history
    elseif ($delta == 3) {
      global $user;
      if ($user->uid == 0) return;  // we don't display stuff for anonymous users
      $item_list = recommender_top_prediction('voting_rec_item2item', $user->uid, $display_num, '_voting_rec_enforce_same_type');
      if (!empty($item_list)) {
        foreach ($item_list as $item) {
          $node = node_load($item['id']);
          $display[] = l($node->title, "/node/{$node->nid}");
        }
        $block['subject'] = t('You might also interest in these nodes based on your previous votes');
        $block['content'] = theme('item_list', $display);
      }
    }

    return $block;
  }
}


// return TRUE if $type is enabled by either Vote Up/Down or Plus1
function _voting_rec_check_content_type($type) {
  if (module_exists('vote_up_down')) {
    $types = variable_get('vote_up_down_node_types', array());
  }
  elseif (module_exists('plus1')) {
    $types = variable_get('plus1_nodetypes', array());
  }
  return in_array($type, $types);
}


// the callback function about enforcing the same type.
function _voting_rec_enforce_same_type($item) {
  if ($current_node = menu_get_object()) {
    $suggest_node = node_load($item['id']);
    if ($suggest_node == FALSE || $suggest_node->status != 1) return FALSE; // the suggested node has to be published.
    if (variable_get('voting_rec_same_type', 1)) { // if enforce same type
      return $suggest_node->type == $current_node->type;
    } else {
      return TRUE;
    }
  } else {
    return FALSE;
  }
}


/**
 * Implementation of hook_run_recommender() for RecAPI
 * Note: this is different from Fivestar
 */
function voting_rec_run_recommender() {
  $sql = "SELECT uid, content_id, value FROM {votingapi_vote} WHERE content_type='node' AND value_type='points' AND tag='vote' AND uid<>0";

  if (variable_get('voting_rec_enable_user2user', 1)) {
    // hack: #510222, add a fake item for user2user, each user votes 0 for that item.
    $sql_hack = $sql . " UNION ALL SELECT DISTINCT uid, 0 AS content_id, 0 AS value FROM {votingapi_vote} WHERE content_type='node' AND value_type='points' AND tag='vote' AND uid<>0";

    watchdog('voting_rec', "Computing recommendation for user2user algorithm");
    // run recommender
    recommender_user2user('voting_rec_user2user', $sql_hack, 'uid', 'content_id', 'value', array(
          'missing'=>'zero', 'performance' => 'memory', 'duplicate' => 'remove', 'lowerbound' => 0, 'knn' => 10));
  }
  if (variable_get('voting_rec_enable_item2item', 1)) {
    // hack: #510222, add a fake user for item2item, each item gets 0 from that user.
    $sql_hack = $sql . " UNION ALL SELECT DISTINCT 0 AS uid, content_id, 0 AS value FROM {votingapi_vote} WHERE content_type='node' AND value_type='points' AND tag='vote' AND uid<>0";

    watchdog('voting_rec', "Computing recommendation for item2item algorithm");
    // run recommender
    recommender_item2item('voting_rec_item2item', $sql_hack, 'uid', 'content_id', 'value', array(
          'missing'=>'zero', 'performance' => 'memory', 'duplicate' => 'remove', 'lowerbound' => 0, 'knn' => 10));
  }
}
