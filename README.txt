README -- Points Voting Recommende

This module is to make node recommendations based on Vote Up/Down or Plus 1.
For more information, please visit the project page at:
http://drupal.org/project/voting_rec

Install/Configure:
1. Download and unzip the module to DRUPAL/sites/all/modules.
2. Enable the module.
3. Go to admin/settings/voting_rec and configure the module.
4. Go to admin/settings/recommender and run recommender.
5. Enable the blocks.

Enjoy.
